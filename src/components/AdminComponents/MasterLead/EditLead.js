import React, { useState, useEffect, useCallback } from "react";
import Box from "@mui/material/Box";
import { useParams, useLocation } from "react-router-dom";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import axios from "axios";
import {
  setKey,
  setDefaults,
  setLanguage,
  setRegion,
  fromAddress,
  fromLatLng,
  fromPlaceId,
  setLocationType,
  geocode,
  RequestType,
} from "react-geocode";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";

export default function FormPropsTextFields() {
  const location = useLocation();
  const lead_id = location.state.id_lead;
  const url = process.env.REACT_APP_API_URL;
  const [namalead, setNamalead] = useState("");
  const [namaperusahaan, setNamaperusahaan] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nohp, setNohp] = useState("");
  const [email, setEmail] = useState("");
  const [latitude, setLatitude] = useState();
  const [longitude, setLongitude] = useState();
  const [pass, setPass] = useState("");

  setDefaults({
    key: `${process.env.REACT_APP_MAPS_API}`, // Your API key here.
    language: "id", // Default language for responses.
    region: "id", // Default region for responses.
  });

  const handleChangeinput = useCallback((event) => {
    if (event.target.name === "namalead") setNamalead(event.target.value);
    else if (event.target.name === "alamat") {
      setAlamat(event.target.value);
      fromAddress(event.target.value)
        .then(({ results }) => {
          const { lat, lng } = results[0].geometry.location;
          setLatitude(lat);
          setLongitude(lng);
        })
        .catch(console.error);
    } else if (event.target.name === "nohp") setNohp(event.target.value);
    else if (event.target.name === "email") setEmail(event.target.value);
    else if (event.target.name === "pass") setPass(event.target.value);
    else if (event.target.name === "namaperusahaan")
      setNamaperusahaan(event.target.value);
  }, []);

  const getDataByID = useCallback(async (leadId) => {
    const { data } = await axios.get(`${url}/lead/${leadId}`);
    setNamalead(data.data[0].nama_lead);
    setNamaperusahaan(data.data[0].nama_toko);
    setEmail(data.data[0].email_lead);
    setNohp(data.data[0].nohp_lead);
    setAlamat(data.data[0].alamat_lead);
    setPass(data.data[0].password_lead);
    setLatitude(data.data[0].lat_lead);
    setLongitude(data.data[0].lng_lead);
  }, []);

  useEffect(() => {
    if (lead_id) getDataByID(lead_id);
  }, [lead_id]);

  const handleSubmit = useCallback(async () => {
    const { status, data } = await axios({
      method: "put",
      url: `${url}/lead/${lead_id}`,
      data: {
        nama_lead: namalead,
        nama_toko: namaperusahaan,
        alamat_lead: alamat,
        nohp_lead: nohp,
        email_lead: email,
        lat_lead: latitude,
        long_lead: longitude,
        password_lead: pass,
      },
    });
    if (status === 200) {
      alert(data.message);
      window.location.reload(false);
    }
  }, [
    namalead,
    alamat,
    nohp,
    email,
    namaperusahaan,
    pass,
    longitude,
    latitude,
  ]);
  return (
    <Box
      component="form"
      sx={{
        "& .MuiTextField-root": { m: 1, width: "25ch" },
      }}
      Validate
      autoComplete="off"
    >
      <h2>Edit Lead</h2>
      <div>
        <TextField
          required
          id="outlined-required"
          name="namalead"
          label="Nama Lead"
          value={namalead}
          onChange={handleChangeinput}
        />
      </div>
      <div>
        <TextField
          required
          id="outlined-required"
          label="Nama Toko"
          name="namaperusahaan"
          value={namaperusahaan}
          onChange={handleChangeinput}
        />
      </div>
      <div>
        <TextField
          required
          id="outlined-required"
          name="alamat"
          label="Alamat"
          value={alamat}
          onChange={handleChangeinput}
        />
      </div>
      <div>
        <TextField
          required
          id="outlined-required"
          label="Nomor HP"
          name="nohp"
          value={nohp}
          onChange={handleChangeinput}
        />
      </div>
      <div>
        <TextField
          required
          id="outlined-required"
          label="Email"
          name="email"
          value={email}
          onChange={handleChangeinput}
        />
      </div>
      <div>
        <TextField
          required
          id="outlined-required"
          label="Password"
          name="pass"
          value={pass}
          onChange={handleChangeinput}
        />
      </div>
      <Button onClick={handleSubmit} variant="contained">
        Simpan Perubahan
      </Button>
    </Box>
  );
}
