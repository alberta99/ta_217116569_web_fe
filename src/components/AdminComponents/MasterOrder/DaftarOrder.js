import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { visuallyHidden } from "@mui/utils";
import moment from "moment";

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "id_order",
    numeric: false,
    disablePadding: true,
    label: "ID order",
  },
  {
    id: "tanggal_order",
    numeric: false,
    disablePadding: false,
    label: "Tanggal Order",
  },
  {
    id: "nama_lead",
    numeric: false,
    disablePadding: false,
    label: "Nama Customer",
  },
  {
    id: "nama_sales",
    numeric: false,
    disablePadding: false,
    label: "Nama Sales",
  },
  {
    id: "nama_toko",
    numeric: false,
    disablePadding: false,
    label: "Nama toko",
  },
  {
    id: "qty_total",
    numeric: true,
    disablePadding: false,
    label: "Jumlah Barang",
  },
  {
    id: "total_order",
    numeric: true,
    disablePadding: false,
    label: "Total",
  },
  {
    id: "status",
    numeric: true,
    disablePadding: false,
    label: "Status",
  },
  {
    id: "btn1",
    numeric: false,
    disablePadding: false,
    label: "",
  },
  {
    id: "btn2",
    numeric: false,
    disablePadding: false,
    label: "",
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function EnhancedTableToolbar(props) {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      <Typography
        sx={{ flex: "1 1 100%" }}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        Master Order
      </Typography>
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

export default function EnhancedTable() {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const url = process.env.REACT_APP_API_URL;
  const [dataLead, setData] = useState([]);
  const [mode, setMode] = useState("online");
  var idLocale = require("moment/locale/id");
  moment.locale("id,", idLocale);
  const navigate = useNavigate();
  //get all data lead
  useEffect(() => {
    axios
      .get(`${url}/laporan/allorder`)
      .then((response) => {
        setData(response.data.data);
        localStorage.setItem("daftarOrder", JSON.stringify(response.data.data));
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        let col = localStorage.getItem("daftarOrder");
        setData(JSON.parse(col));
        setMode("offline");
      });
  }, []);

  const onDetailClick = useCallback((orderId) => {
    navigate("/admin/detailorder/", { state: { order_id: orderId } });
  });

  const onSelesaiClick = useCallback((orderId) => {
    axios
      .put(`${url}/order/updatestatus/${orderId}`)
      .then((response) => {
        setData(response.data.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      }, []);
    window.location.reload();
  });

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - dataLead.length) : 0;

  const visibleRows = React.useMemo(
    () =>
      stableSort(dataLead, getComparator(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      ),
    [dataLead, order, orderBy, page, rowsPerPage]
  );

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = dataLead.map((n) => n.name);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };
  return (
    <Box sx={{ width: "100%" }}>
      <div>
        {mode === "offline" ? (
          <div
            style={{
              padding: 5,
              backgroundColor: "yellow",
              textAlign: "center",
              fontWeight: "bold",
              color: "red",
              marginBottom: 10,
            }}
          >
            Tidak ada koneksi internet, Anda sedang offline!
          </div>
        ) : null}
      </div>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <TableContainer>
          <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={dataLead.length}
            />
            <TableBody>
              {visibleRows.map((row, index) => {
                return (
                  <TableRow>
                    <TableCell>{row.id_order}</TableCell>
                    <TableCell>{row.tanggal_order}</TableCell>
                    <TableCell>{row.nama_lead}</TableCell>
                    <TableCell>{row.nama_sales}</TableCell>
                    <TableCell>{row.nama_toko}</TableCell>
                    <TableCell>{row.qty_total}</TableCell>
                    <TableCell>Rp {row.total_order}</TableCell>
                    <TableCell>
                      {row.status == 1 ? "Selesai" : "Diproses"}
                    </TableCell>
                    <TableCell>
                      <Button
                        variant="contained"
                        onClick={() => onDetailClick(row.id_order)}
                      >
                        Detail
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button
                        variant="contained"
                        onClick={() => onSelesaiClick(row.id_order)}
                      >
                        Selesaikan Order
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 33 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={dataLead.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
}
