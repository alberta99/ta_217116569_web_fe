import React, { useState, useEffect } from "react";
import { useParams, useLocation } from "react-router-dom";
import "./InvoicePage.css";
import axios from "axios";
import moment from "moment";
var idLocale = require("moment/locale/id");
moment.locale("id,", idLocale);

function InvoicePage() {
  const url = process.env.REACT_APP_API_URL;
  const [invoiceData, setInvoiceData] = useState([]);
  const [invoiceDataTable, setInvoiceDataTable] = useState([]);
  const location = useLocation();
  const order_id = location.state.order_id;

  useEffect(() => {
    const fetchInvoiceData = async () => {
      try {
        const detailResponse = await axios.get(
          `${url}/order/detailorder/${order_id}`
        );
        const sumResponse = await axios.get(
          `${url}/order/ordersumbyid/${order_id}`
        );

        setInvoiceDataTable(detailResponse.data.data);
        setInvoiceData(sumResponse.data.data);
        localStorage.setItem(
          "dataInvoice",
          JSON.stringify(sumResponse.data.data)
        );
        localStorage.setItem(
          "dataInvoiceTable",
          JSON.stringify(detailResponse.data.data)
        );
      } catch (err) {
        console.log(err);
        let col = localStorage.getItem("dataInvoice");
        setInvoiceData(JSON.parse(col));
        let col2 = localStorage.getItem("dataInvoiceTable");
        setInvoiceDataTable(JSON.parse(col2));
      }
    };

    fetchInvoiceData();
  }, [order_id]);

  if (!invoiceData || !invoiceDataTable) {
    return <div>Loading...</div>;
  }

  return (
    <div className="invoice-container">
      <h2 className="invoice-title">Purchase Order</h2>
      <p>Nomor: {invoiceData.id_order}</p>
      <p>Customer: {invoiceData.nama_lead + " - " + invoiceData.nama_toko}</p>
      <p>
        Tanggal & Waktu Order :{" "}
        {moment(invoiceData.tanggal_order).locale("id").format("LLLL")}
      </p>
      <p>
        Tanggal kirim :{" "}
        {moment(invoiceData.tanggal_kirim).locale("id").format("LL")}
      </p>
      <p>Alamat kirim: {invoiceData.alamat_kirim}</p>
      <p>Sales: {invoiceData.nama_sales}</p>
      <p>Pembayaran: {invoiceData.jenis_pembayaran}</p>
      <table className="invoice-table">
        <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Harga Barang</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {invoiceDataTable.map((item) => (
            <tr key={item.id_order}>
              <td>{item.nama_barang}</td>
              <td>{item.qty_barang}</td>
              <td>Rp{item.harga_barang_order}</td>
              <td>Rp{item.qty_barang * item.harga_barang_order}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <p className="invoice-total">Subtotal: Rp{invoiceData.sub_total_order}</p>
      <p className="invoice-total">Diskon: Rp{invoiceData.harga_diskon}</p>
      <p className="invoice-total">Total: Rp{invoiceData.total_order}</p>
    </div>
  );
}

export default InvoicePage;
