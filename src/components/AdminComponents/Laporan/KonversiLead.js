import React, { useState, useEffect } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import DeleteIcon from "@mui/icons-material/Delete";
import FilterListIcon from "@mui/icons-material/FilterList";
import { visuallyHidden } from "@mui/utils";
import DatePicker, { registerLocale, setDefaultLocale } from "react-datepicker";
import { id } from "date-fns/locale/id";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "nama_sales",
    numeric: true,
    disablePadding: false,
    label: "Nama sales",
  },
  {
    id: "lead",
    numeric: true,
    disablePadding: false,
    label: "Jumlah lead",
  },
  {
    id: "cust",
    numeric: true,
    disablePadding: false,
    label: "Jumlah konversi ke customer",
  },
  {
    id: "conversion_rate",
    numeric: true,
    disablePadding: false,
    label: "Tingkat konversi",
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function EnhancedTableToolbar(props) {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: "1 1 100%" }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Laporan konversi lead
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton>
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

export default function EnhancedTable() {
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const url = process.env.REACT_APP_API_URL;
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [page, setPage] = React.useState(0);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [mode, setMode] = useState("online");

  const allData = () => {
    axios
      .get(`${url}/laporan/conversionrate`)
      .then((response) => {
        setData(response.data.data[0]);
        localStorage.setItem(
          "datakonversi1",
          JSON.stringify(response.data.data[0])
        );
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        let col = localStorage.getItem("datakonversi1");
        setData(JSON.parse(col));
        setMode("offline");
      });
    axios
      .get(`${url}/laporan/conversionratebysales`)
      .then((response) => {
        setData2(response.data.data);
        localStorage.setItem(
          "datakonversi2",
          JSON.stringify(response.data.data)
        );
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        let col = localStorage.getItem("datakonversi2");
        setData2(JSON.parse(col));
      });
  };
  useEffect(() => {
    allData();
  }, []);

  const handleTanggal = async () => {
    if (!startDate || !endDate) {
      alert("Mohon pilih tanggal mulai dan tanggal akhir");
      return;
    }
    const tgl_start = moment(startDate).format("YYYY-MM-DD");
    const tgl_end = moment(endDate).format("YYYY-MM-DD");
    try {
      const response = await axios.get(
        `${url}/laporan/konversibysalestanggal/${tgl_start}/${tgl_end}`
      );
      setData2(response.data.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data2.length) : 0;

  const visibleRows = React.useMemo(
    () =>
      stableSort(data2, getComparator(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      ),
    [order, orderBy, page, rowsPerPage, data2]
  );

  registerLocale("id", id);

  return (
    <Box sx={{ width: "100%" }}>
      <div>
        {mode === "offline" ? (
          <div
            style={{
              padding: 5,
              backgroundColor: "yellow",
              textAlign: "center",
              fontWeight: "bold",
              color: "red",
              marginBottom: 10,
            }}
          >
            Tidak ada koneksi internet, Anda sedang offline!
          </div>
        ) : null}
      </div>
      <div>
        <div style={{ marginBottom: 10, width: "100%" }}>
          Pilih rentang tanggal :
          <DatePicker
            selected={startDate}
            onChange={(date) => setStartDate(date)}
            selectsStart
            startDate={startDate}
            endDate={endDate}
            locale="id"
            dateFormat="dd-MM-yyyy"
          />
          -
          <DatePicker
            selected={endDate}
            onChange={(date) => setEndDate(date)}
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            minDate={startDate}
            locale="id"
            dateFormat="dd-MM-yyyy"
          />
          <button
            onClick={handleTanggal}
            style={{ marginLeft: 10, width: "10%" }}
          >
            Filter
          </button>
        </div>{" "}
        Persentase Konversi Lead ke Customer :{" "}
        {(
          (parseInt(data.cust) / (parseInt(data.cust) + parseInt(data.lead))) *
          100
        ).toFixed(2)}
        %
      </div>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <TableContainer>
          <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={data2.length}
            />
            <TableBody>
              {visibleRows.map((row, index) => {
                return (
                  <TableRow>
                    <TableCell align="right">{row.nama_sales}</TableCell>
                    <TableCell align="right">{row.lead}</TableCell>
                    <TableCell align="right">{row.cust}</TableCell>
                    <TableCell align="right">
                      {((row.cust / (row.lead + row.cust)) * 100
                        ? (row.cust / (row.lead + row.cust)) * 100
                        : 0
                      ).toFixed(2)}
                      %
                    </TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 33 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data2.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
}
