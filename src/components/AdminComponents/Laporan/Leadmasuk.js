import React, { useEffect, useState, useMemo } from "react";
import TableContainer from "../Laporan/TableContainer";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  SelectColumnFilter,
  DateRangeColumnFilter,
  dateBetweenFilterFn,
} from "../Laporan/filters";
import axios from "axios";

const LeadMasuk = () => {
  const url = process.env.REACT_APP_API_URL;
  const [rowData, setRowData] = useState([]);
  const [mode, setMode] = useState("online");

  useEffect(() => {
    axios
      .get(`${url}/laporan/laporanleadmasuk`)
      .then((response) => {
        setRowData(response.data.data);
        localStorage.setItem(
          "dataleadmasuk",
          JSON.stringify(response.data.data)
        );
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        let col = localStorage.getItem("dataleadmasuk");
        setRowData(JSON.parse(col));
        setMode("offline");
      });
  }, []);
  const columns = useMemo(
    () => [
      {
        Header: "Nama Lead",
        accessor: "nama_lead",
      },
      {
        Header: "Nama Salesperson",
        accessor: "nama_sales",
        Filter: SelectColumnFilter,
      },
      {
        Header: "Tanggal Bergabung",
        accessor: "tgl_join_lead",
        Filter: DateRangeColumnFilter,
        filter: dateBetweenFilterFn,
      },
    ],
    []
  );
  return (
    <div>
      <div>
        {mode === "offline" ? (
          <div
            style={{
              padding: 5,
              backgroundColor: "yellow",
              textAlign: "center",
              fontWeight: "bold",
              color: "red",
              marginBottom: 10,
            }}
          >
            Tidak ada koneksi internet, Anda sedang offline!
          </div>
        ) : null}
      </div>
      <h2>Laporan Lead Masuk</h2>
      <TableContainer columns={columns} data={rowData} />
    </div>
  );
};

export default LeadMasuk;
