// src/components/Navbar.js
import React, { Fragment, useState } from "react";
import { Link, NavLink, Outlet, Routes, Route } from "react-router-dom";
import "./Navbar.css";
import DaftarOrderCust from "./daftarorder";
import DaftarProdukCust from "./daftarproduk";
import DetailProduk from "./detailproduk";
import DetailOrder from "./detailorder";
import GantiPassword from "./gantipassword";
import Editprofile from "./editprofile";

const NavBar = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <Fragment>
      <nav className="navbar">
        <h1>MyApp</h1>
        <div className={`nav-links ${menuOpen ? "active" : ""}`}>
          <NavLink
            to="/customer/daftarproduk"
            activeClassName="active"
            onClick={toggleMenu}
          >
            Daftar Produk
          </NavLink>
          <NavLink
            to="/customer/daftarorder"
            activeClassName="active"
            onClick={toggleMenu}
          >
            Daftar Order
          </NavLink>
          <NavLink
            to="/customer/editprofile"
            activeClassName="active"
            onClick={toggleMenu}
          >
            Edit Profile
          </NavLink>
          <NavLink
            to="/"
            activeClassName="active"
            onClick={() => window.sessionStorage.removeItem("lead")}
          >
            Logout
          </NavLink>
        </div>
        <div
          className={`menu-toggle ${menuOpen ? "active" : ""}`}
          onClick={toggleMenu}
        >
          <div className="line1"></div>
          <div className="line2"></div>
          <div className="line3"></div>
        </div>
      </nav>
      <Routes>
        <Route path="daftarproduk" element={<DaftarProdukCust />} />
        <Route path="daftarorder" element={<DaftarOrderCust />} />
        <Route path="detailorder/:order_id" element={<DetailOrder />} />
        <Route path="detailproduk/:id_barang" element={<DetailProduk />} />
        <Route path="gantipassword" element={<GantiPassword />} />
        <Route path="editprofile" element={<Editprofile />} />
      </Routes>
    </Fragment>
  );
};

export default NavBar;
