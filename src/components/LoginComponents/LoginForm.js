import InputField from "./InputField";
import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const url = process.env.REACT_APP_API_URL;
  const navigate = useNavigate();

  useEffect(() => {
    if (window.sessionStorage.getItem("lead") != null) {
      if (window.sessionStorage.getItem("lead") == "admin") {
        navigate("/admin");
      } else {
        navigate("/customer");
      }
    } else {
      navigate("/");
    }
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (email == "admintokojayaabadi@gmail.com" && password == "jayaabadi123") {
      window.sessionStorage.setItem("lead", "admin");
      navigate("/admin");
    } else {
      try {
        const res = await axios.post(`${url}/lead/login`, {
          email,
          password,
        });
        if (res.status === 200) {
          console.log(res.status);
          window.sessionStorage.setItem("lead", res.data.data.id_lead);
          navigate("/customer");
        } else {
          setMessage("Login gagal silahkan periksa email atau password anda!");
        }
      } catch (error) {
        setMessage("Login gagal silahkan periksa email atau password anda!");
      }
    }
  };

  return (
    <div className="login-form">
      <h2>Login</h2>
      <InputField
        type="text"
        placeholder="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <InputField
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      {message && <p>{message}</p>}
      <button onClick={handleSubmit}>Login</button>
    </div>
  );
};

export default Login;
